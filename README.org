#+TITLE: Projet Réseau Social Inter-Miage
[[file:./Images/mastodon.png]]

* Table des matières                                      :TOC_4_gh:noexport:
- [[#team][Team]]
- [[#contexte][Contexte]]
- [[#présentation-de-mastodon][Présentation de Mastodon]]
- [[#déploiement-mastodon][Déploiement Mastodon]]
  - [[#initialisation-avec-ovh][Initialisation avec OVH]]
  - [[#putty][PuTTY]]
    - [[#1ère-étape][1ère étape]]
- [[#déploiement-peertube][Déploiement Peertube]]
- [[#mise-en-place-activitypub][Mise en place ActivityPub]]

* Team
- Inkaran Thuraiyappah
- Ganeistan Sinnadurai
- Victoria Pereira
- Hakan Gunes

* Contexte
Mastodon est à la fois un logiciel libre de microblogage, et un réseau social décentralisé, créé en 2016, qui permet de publier des messages.
Le réseau est le plus souvent présenté dans les médias par ses différences vis-à-vis de Twitter.
 Alors que sur Twitter, les messages ont été longtemps limités à 280 caractères, ils peuvent afile:./peertube.pngtteindre 500 caractères sur Mastodon. 

Notre projet consistera à mettre en place un réseau social dédié aux Miagist/Users/allfire77/Desktop/MesProjets/Mastodon/reseau-social-intermiages/tuto1.png]]es de l'ensemble du territoire français à l'aide de Mastodon et de PeerTube,
un service de partage de vidéo décentralisé en Open Source semblable à Youtube.
Le but du projet est de créer une instance par Miage et permettre la collaboration entre elles.
Ce projet sera encadré par notre responsable de la filière Miage d'Evry, Didier Courtaud et le directeur de la Miage de Nice, Michel Buffa.


* Présentation de Mastodon
Mastodon est un service de microblogging comparable à twitter.

Avantages de Mastodon par rapport à Twitter:
-	Créer sa propre instance de Mastodon: on a donc «son propre réseau» que l’on peut
-	Pas de publicité

Mastodon est programme décentralisé, il fonctionne avec une multitude de serveurs reliés entre eux.

* Déploiement Mastodon

** Initialisation avec OVH
	 Aller sur son profil OVH
   Barre du haut -> Cloud
   Volet de gauche -> Serveurs 

  #+ATTR_LATEX: :width 0.7\textwidth :placement [h]
   #+LABEL: cr-model
   [[file:./tuto1.png]]

  Un serveur est caractérisé par :
  - Son adresse IP (IPv4 / IPv6)
  - Son nom de domaine

  Tous les serveurs possèdent une porte d’entrée : c’est l’accès root
  C’est avec celui-ci qu’on peut gérer notre serveur
  Pour accéder au root de notre serveur, il nous faut 2 informations:
   - L’identifiant: adresse ipV4 ou ipv6
   - Mot de passe


   Le VPS étant distant, il faut trouver un moyen de se connecter au serveur via son poste personnel
   Pour communiquer avec son serveur, il faut passer par le protocole SSH.
   Le terminal Windows ne gère pas la communication SSH.

** PuTTY
   PuTTY permet de se connecter à un VPS via le protocole SSH via une machine Windows.

*** 1ère étape
    - Lancer PuTTY
    - Entrer l’adresse IP
    - Cliquer sur Open
    - Un terminal se lance

   #+ATTR_LATEX: :width 0.9\textwidth :placement [h]
   #+LABEL: cr-model
   [[file:./Images/tuto2.png]]
* Déploiement Peertube
  #+ATTR_LATEX: :width 0.9\textwidth :placement [h]
  #+LABEL: cr-model
  [[file:./Images/peertube.png]]
* Mise en place ActivityPub
  #+ATTR_LATEX: :width 0.9\textwidth :placement [h]
  #+LABEL: cr-model
  [[file:./Images/activitypub.png]]
